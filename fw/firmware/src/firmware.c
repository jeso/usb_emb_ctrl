/* System */
#include "chip.h"
#include "stdint.h"
#include <cr_section_macros.h>

/* Project */
#include "periph_usb.h"

void initBoard(void){
	// Switch to IRC clock (not PLL! motherfucker...), enable xtal, setup, update core clock
	Chip_Clock_SetMainClockSource(SYSCTL_MAINCLKSRC_IRC);
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_IOCON);
	Chip_IOCON_PinMuxSet(LPC_IOCON, 2, 0, IOCON_FUNC1 | IOCON_MODE_INACT | IOCON_ADMODE_EN);		// XTAL Pin1
	Chip_IOCON_PinMuxSet(LPC_IOCON, 2, 1, IOCON_FUNC1 | IOCON_MODE_INACT | IOCON_ADMODE_EN);		// XTAL Pin2
	Chip_SetupXtalClocking();
	SystemCoreClockUpdate();

	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_IOCON);

    Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 26, IOCON_FUNC0 | IOCON_MODE_INACT);	// Debug LED Green
	Chip_GPIO_SetPinDIR(LPC_GPIO, 1, 26, 1);

	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 27, IOCON_FUNC0 | IOCON_MODE_INACT); // Debug LED Green
	Chip_GPIO_SetPinDIR(LPC_GPIO, 1, 27, 1);

	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 3, IOCON_FUNC1 | IOCON_MODE_INACT);		// USB VBUS

	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 18, IOCON_FUNC2 | IOCON_MODE_INACT);		// FAN1 PWM HARD CT32B0_MAT0
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 19, IOCON_FUNC2 | IOCON_MODE_INACT);		// FAN2 PWM HARD CT32B0_MAT1
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 1, IOCON_FUNC2 | IOCON_MODE_INACT);		// FAN3 PWM HARD CT32B0_MAT2

	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 0, IOCON_FUNC1 | IOCON_MODE_INACT);		// FAN1 PWM SOFT CT32B1_MAT0
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 16, IOCON_FUNC2 | IOCON_MODE_INACT);		// FAN2 PWM SOFT CT32B1_MAT3
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 14, IOCON_FUNC3 | IOCON_MODE_INACT);		// FAN3 PWM SOFT CT32B1_MAT1

	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 21, IOCON_FUNC3 | IOCON_MODE_INACT);		// FAN1 Sense CT16B0_CAP2
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 20, IOCON_FUNC1 | IOCON_MODE_INACT);		// FAN2 Sense CT16B1_CAP0
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 2, IOCON_FUNC2 | IOCON_MODE_INACT);		// FAN3 Sense CT16B0_CAP0

	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 4, IOCON_FUNC1 | IOCON_MODE_INACT);		// I2C0 SCL
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 5, IOCON_FUNC1 | IOCON_MODE_INACT);		// I2C0 SDA
	Chip_IOCON_PinMuxSet(LPC_IOCON, 0, 7, IOCON_FUNC3 | IOCON_MODE_INACT);		// I2C1 SCL
	Chip_IOCON_PinMuxSet(LPC_IOCON, 1, 24, IOCON_FUNC2 | IOCON_MODE_INACT);		// I2C1 SDA

	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_GPIO);
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_CT32B1);
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_CT16B0);
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_CT16B1);
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_I2C0);
	Chip_Clock_EnablePeriphClock(SYSCTL_CLOCK_I2C1);

	Chip_USB_Init();
}

void initCT32B1(void){	// Soft FAN control
	Chip_TIMER_Init(LPC_TIMER32_1);				// Enable clocks
	Chip_TIMER_Reset(LPC_TIMER32_1);			// Reset
	Chip_TIMER_PrescaleSet(LPC_TIMER32_1, 8);	// Timer clock 6MHz (25kHz 256-step PWM)

	Chip_TIMER_SetMatch(LPC_TIMER32_1, 2, 255);	// PWM Cycle length
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER32_1, 2);

	Chip_TIMER_SetMatch(LPC_TIMER32_1, 0, 255);	// 0% duty (inv)
	LPC_TIMER32_1->PWMC |= 0x1;	// Enable PWM channel 0 (Soft FAN 1)

	Chip_TIMER_SetMatch(LPC_TIMER32_1, 1, 255);
	LPC_TIMER32_1->PWMC |= 0x2;	// Enable PWM channel 1 (SOFT FAN 3)

	Chip_TIMER_SetMatch(LPC_TIMER32_1, 3, 255);
	LPC_TIMER32_1->PWMC |= 0x8;	// Enable PWM channel 3 (SOFT FAN 2)

	Chip_TIMER_Enable(LPC_TIMER32_1);			// Enable timer
}

void initCT32B0(void){	// HARD FAN control
	Chip_TIMER_Init(LPC_TIMER32_0);				// Enable clocks
	Chip_TIMER_Reset(LPC_TIMER32_0);			// Reset
	Chip_TIMER_PrescaleSet(LPC_TIMER32_0, 8);	// Timer clock 6MHz (25kHz 256-step PWM)

	Chip_TIMER_SetMatch(LPC_TIMER32_0, 3, 255);	// PWM Cycle length
	Chip_TIMER_ResetOnMatchEnable(LPC_TIMER32_0, 3);

	Chip_TIMER_SetMatch(LPC_TIMER32_0, 0, 255);	// 0% duty (inv)
	LPC_TIMER32_0->PWMC |= 0x1;	// Enable PWM channel 0 (HARD FAN 1)

	Chip_TIMER_SetMatch(LPC_TIMER32_0, 1, 255);
	LPC_TIMER32_0->PWMC |= 0x2;	// Enable PWM channel 1 (HARD FAN 2)

	Chip_TIMER_SetMatch(LPC_TIMER32_0, 2, 255);
	LPC_TIMER32_0->PWMC |= 0x4;	// Enable PWM channel 2 (HARD FAN 3)

	Chip_TIMER_Enable(LPC_TIMER32_0);			// Enable timer
}

int main(void) {

	int i,j;

	initBoard();
	initCT32B1();
	initCT32B0();

	i=0;

	while(1){

		Chip_TIMER_SetMatch(LPC_TIMER32_0, 0, i);	// 0% duty (inv)
		i++;
		if(i==256) i=0;

		for(j=0x4FFFF; j>0; j--);

    }

    return 0 ;
}
