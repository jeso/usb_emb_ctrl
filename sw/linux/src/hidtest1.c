#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <malloc.h>

#include <hidapi/hidapi.h>
#include <usb.h>

#define COLOR_NRM	"\x1B[0m"
#define COLOR_RED	"\x1B[31m"
#define COLOR_GRN	"\x1B[32m"
#define COLOR_YEL	"\x1B[33m"

#define USB_ID_VENDOR_KONTRON	0x208b
#define USB_ID_PRODUCT_P110	0x002b	// FV-LDS-MB
#define USB_ID_PRODUCT_P117	0x002c	// FV-LDS-DABGRN

int _VERBOSE = 0;

void wprintf_g(const wchar_t *format, ...) {
	wprintf(L"%s", COLOR_GRN);
	va_list arglist;
	va_start( arglist, format);
	vwprintf(format, arglist);
	va_end(arglist);
	wprintf(L"%s", COLOR_NRM);
}

void wprintf_y(const wchar_t *format, ...) {
	wprintf(L"%s", COLOR_YEL);
	va_list arglist;
	va_start( arglist, format);
	vwprintf(format, arglist);
	va_end(arglist);
	wprintf(L"%s", COLOR_NRM);
}

void wprintf_r(const wchar_t *format, ...) {
	wprintf(L"%s", COLOR_RED);
	va_list arglist;
	va_start( arglist, format);
	vwprintf(format, arglist);
	va_end(arglist);
	wprintf(L"%s", COLOR_NRM);
}

hid_device * hidOpenDevice(unsigned short vid, unsigned short pid) {

	wchar_t sBuffer[256];
	hid_device * hidDevHandle;
	struct hid_device_info * hidDevInfo;

	if(hid_init() != 0) {
		wprintf_r(L"[!] Cannot initialize HID library\n");
		return NULL;
	} else {
		if(_VERBOSE) wprintf_g(L"[i] HID library initialized\n");
	}

	hidDevInfo = hid_enumerate(vid,pid);
	if(hidDevInfo == NULL) {
		wprintf(L"Cannot find specified device");
		return NULL;
	}

	if(hidDevInfo->next != NULL) {
		if(_VERBOSE) wprintf_y(L"[i] Found more than one device, using first one (S/N %S)", hidDevInfo->serial_number);
	} else {
		if(_VERBOSE) wprintf_g(L"[i] Found exactly one device (S/N %S)\n", hidDevInfo->serial_number);
	}

	if(_VERBOSE) wprintf(L"%s[i] Current device: %04x %04x %s\n", COLOR_YEL, hidDevInfo->vendor_id, hidDevInfo->product_id, COLOR_NRM);

	hidDevHandle = hid_open(hidDevInfo->vendor_id, hidDevInfo->product_id, NULL);

	if(hidDevHandle == NULL) {
		wprintf(L"Cannot open device! (insufficient privileges?)\n");
	} else {
		if(_VERBOSE) {
			hid_get_manufacturer_string(hidDevHandle, sBuffer, 255);
			wprintf_y(L"[i] MFG String: %S\n", sBuffer);
			hid_get_product_string(hidDevHandle, sBuffer, 255);
			wprintf_y(L"[i] PRO String: %S\n", sBuffer);
		}
	}
	return hidDevHandle;
}

int main(int argc, char *argv[]) {

	int i;

	if(argc > 0) {
		for(i=0; i<argc; i++) {
			if(!strcmp(argv[i], "-v")) {
				_VERBOSE = 1;
				wprintf_y(L"[i] Verbose mode!\n");
			}
		}
	}

	hid_device * hidDevHandle = hidOpenDevice(USB_ID_VENDOR_KONTRON, USB_ID_PRODUCT_P117);

	const unsigned char hidReport[7] = {0x21, 0, 0, 0, 1, 0, 0};
	const unsigned int  hidReportLen = 7;
	wprintf_y(L"[i] Written bytes: %d\n", hid_write(hidDevHandle, hidReport, hidReportLen));

	unsigned char * hidReadBuffer = (unsigned char *)malloc(64 * sizeof(unsigned char));

	wprintf_y(L"[i] Read bytes: %d\n", hid_read(hidDevHandle, hidReadBuffer, 7));
	for(i=0; i<7; i++){
		wprintf_y(L"[i] Contents byte %d: %#x\n", i, hidReadBuffer[i]);
	}

	i = hid_read_timeout(hidDevHandle, hidReadBuffer, 7, 100);
	if(i>0){
		wprintf_r(L"[!] Additional %d bytes read from buffer!\n", i);
	}
	else if(i == 0){
		wprintf_g(L"[i] No additional bytes were pending.\n", i);
	}
	else {
		wprintf_r(L"[!] Error while reading additional data.\n");
	}

	hid_close(hidDevHandle);
	return(0);
}
